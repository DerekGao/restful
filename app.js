const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const port = process.env.PORT || 3000;

// 中间件，用于解析 POST 请求的 JSON 数据
app.use(bodyParser.json());

// 模拟用户数据
const users = [
  { username: 'user1', password: 'password1' },
  { username: 'user2', password: 'password2' },
];

// 中间件，用于简单的基本认证
const basicAuth = (req, res, next) => {
  const authHeader = req.headers.authorization;

  if (!authHeader) {
    res.status(401).send('Authentication required');
    return;
  }

  const auth = Buffer.from(authHeader, 'base64').toString();
  const [username, password] = auth.split(':');

  const user = users.find((u) => u.username === username && u.password === password);
  if (!user) {
    res.status(401).send('Invalid credentials');
    return;
  }

  // 如果认证成功，将用户信息存储到请求对象中，以供后续路由使用
  req.user = user;
  next();
};


const resources = [
  { id: 1, name: 'Resource 1' },
  { id: 2, name: 'Resource 2' },
];

// GET请求，获取所有资源
app.get('/api/resources', basicAuth, (req, res) => {
  res.send('Authenticated user: ' + req.user.username);
});

// GET请求，获取特定资源
app.get('/api/resources/:id', (req, res) => {
  const resourceId = parseInt(req.params.id);
  const resource = resources.find((r) => r.id === resourceId);
  if (resource) {
    res.json(resource);
  } else {
    res.status(404).json({ message: 'Resource not found' });
  }
});

// POST请求，创建新资源
app.post('/api/resources', (req, res) => {
    const newResource = req.body; // 获取客户端提供的JSON数据
    newResource.id = resources.length + 1; // 分配一个新的唯一ID
    resources.push(newResource); // 将新资源添加到数组
    res.json(newResource); // 返回创建的资源

  res.status(201).json(newResource);
});

// PUT请求，更新特定资源
app.put('/api/resources/:id', (req, res) => {
  const resourceId = parseInt(req.params.id);
  const resource = resources.find((r) => r.id === resourceId);
  if (resource) {
    resource.name = req.body.name;
    res.json(resource);
  } else {
    res.status(404).json({ message: 'Resource not found' });
  }
});

// DELETE请求，删除特定资源
app.delete('/api/resources/:id', (req, res) => {
  const resourceId = parseInt(req.params.id);
  const index = resources.findIndex((r) => r.id === resourceId);
  if (index !== -1) {
    resources.splice(index, 1);
    res.json({ message: 'Resource deleted' });
  } else {
    res.status(404).json({ message: 'Resource not found' });
  }
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
